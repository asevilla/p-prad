/*
 * P-P Rad 1.0
 * Copyright (c) 2019 Instituto Nacional de Cancerología - INC
 * All Right Reserved.
 *
 * Developed by Andrea Giraldo Torres
 *              Andrés Camilo Sevilla Moreno
 *
 * Use and copying of these libraries and preparation of derivative works
 * based upon these libraries are permitted. Any copy of these libraries
 * must include this copyright notice.
 *
 * Bogotá, Colombia.
 *
 */

// ROOT Headers
#include "TROOT.h"
#include "TFile.h"
#include "TH1.h"
#include "TFrame.h"
#include "TCanvas.h"
#include "TLegend.h"
#include "TStyle.h"
#include "TGaxis.h"

#include <map>
#include <iostream>
#include <string>

using namespace std;

void Plot_02(){

	gStyle->SetOptStat(0);

	// Draw histos filled by Geant4 simulation

	TFile* F1 = new TFile("data/panoramic_RX50kVp.root");
	TFile* F2 = new TFile("data/panoramic_RX60kVp.root");
	TFile* F3 = new TFile("data/panoramic_RX70kVp.root");


	TCanvas* c1 = new TCanvas("c1", "c1", 500, 500);

	// --- PhotonEnergySpectrum ---

	c1->cd()->SetLeftMargin(0.15);
	c1->cd()->SetLogy(false);
	c1->cd()->SetGrid(true,true);
	c1->cd()->SetTicks(true,true);

	TH1* h0_1 = static_cast<TH1D*>(F1->Get("histograms/PhotonEnergySpectrum"));
	h0_1->SetLineColor(kGreen);
	h0_1->SetLineWidth(2);
	h0_1->GetXaxis()->SetTitleOffset(1.2);
	h0_1->GetYaxis()->SetTitleOffset(1.8);
	h0_1->SetTitle("Photon energy distribution X-Ray tube");
	h0_1->SetXTitle("Energy [keV]");
	h0_1->SetYTitle("Relative Counts");
	h0_1->Scale(1/h0_1->GetEntries());
	h0_1->Draw("HIST");

	TH1* h0_2 = static_cast<TH1D*>(F2->Get("histograms/PhotonEnergySpectrum"));
	h0_2->SetLineColor(kRed);
	h0_2->SetLineWidth(2);
	h0_2->Scale(1/h0_2->GetEntries());
	h0_2->Draw("HIST same");

	TH1* h0_3 = static_cast<TH1D*>(F3->Get("histograms/PhotonEnergySpectrum"));
	h0_3->SetLineColor(kBlue);
	h0_3->SetLineWidth(2);
	h0_3->Scale(1/h0_3->GetEntries());
	h0_3->Draw("HIST same");

	TLegend* l0 = new TLegend(0.65, 0.70, 0.85, 0.85) ;
	l0->AddEntry(h0_1, "50kVp", "L") ;
	l0->AddEntry(h0_2, "60kVp", "L") ;
	l0->AddEntry(h0_3, "70kVp", "L") ;
	l0->Draw() ;

	// --- End PhotonEnergySpectrum ---

	c1->Print("data/panoramic_001.png");

	// Thyroid

	TCanvas* c2 = new TCanvas("c2", "c2", 1000, 500);
	c2->Divide(2,1);

	// --- EdepPerEvent ---

	c2->cd(1);
	c2->cd(1)->SetLeftMargin(0.15);
	c2->cd(1)->SetLogy(false);
	c2->cd(1)->SetGrid(true,true);

	TH1* h1_1 = static_cast<TH1D*>(F1->Get("histograms/ThyroidEdepPerEvent"));
	h1_1->SetLineColor(kGreen);
	h1_1->SetLineWidth(2);
	h1_1->GetXaxis()->SetTitleOffset(1.2);
	h1_1->GetYaxis()->SetTitleOffset(1.6);
	h1_1->SetTitle("#splitline{Deposited energy per incident photon (Thyroid Tissue)}{by X-Ray tube for panoramic radiography}");
	h1_1->SetXTitle("Deposited energy [keV]");
	h1_1->SetYTitle("Relative Counts");
	h1_1->Scale(1/h1_1->GetEntries());
	h1_1->Draw("HIST");
	c2->cd(1)->Update();

	TH1* h1_2 = static_cast<TH1D*>(F2->Get("histograms/ThyroidEdepPerEvent"));
	h1_2->SetLineColor(kRed);
	h1_2->SetLineWidth(2);
	h1_2->Scale(1/h1_2->GetEntries());
	h1_2->Draw("HIST same");

	TH1* h1_3 = static_cast<TH1D*>(F3->Get("histograms/ThyroidEdepPerEvent"));
	h1_3->SetLineColor(kBlue);
	h1_3->SetLineWidth(2);
	h1_3->Scale(1/h1_3->GetEntries());
	h1_3->Draw("HIST same");

	// --- End EdepPerEvent ---

	// --- DosePerEvent ---

	TH1* h2_1 = static_cast<TH1D*>(F1->Get("histograms/ThyroidDosePerEvent"));
	h2_1->Scale(1/h2_1->GetEntries());
	h2_1->Draw("same");

	Double_t ypos1 = c2->cd(1)->GetFrame()->GetY2();

	//draw an axis on the right side
	TGaxis *axis1 = new TGaxis(gPad->GetUxmin(),ypos1,
			gPad->GetUxmax(), ypos1,0,h2_1->GetXaxis()->GetBinCenter(h2_1->GetXaxis()->GetNbins()),510,"-");
	axis1->SetLineColor(kBlack);
	axis1->SetLabelColor(kBlack);
	axis1->SetTitleOffset(1.2);
	axis1->SetTitle("Absorbed dose [Gy]");
	axis1->Draw();

	TLegend* l1 = new TLegend(0.65, 0.70, 0.85, 0.85) ;
	l1->AddEntry(h1_1, "50kVp", "L") ;
	l1->AddEntry(h1_2, "60kVp", "L") ;
	l1->AddEntry(h1_3, "70kVp", "L") ;
	l1->Draw() ;


	// --- End DosePerEvent ---

	// Lens

	// --- EdepPerEvent ---

	c2->cd(2);
	c2->cd(2)->SetLeftMargin(0.15);
	c2->cd(2)->SetLogy(false);
	c2->cd(2)->SetGrid(true,true);

	TH1* h3_1 = static_cast<TH1D*>(F1->Get("histograms/LensEdepPerEvent"));
	h3_1->SetLineColor(kGreen);
	h3_1->SetLineWidth(2);
	h3_1->GetXaxis()->SetTitleOffset(1.2);
	h3_1->GetYaxis()->SetTitleOffset(1.6);
	h3_1->SetTitle("#splitline{Deposited energy per incident photon (Lens)}{by X-Ray tube for panoramic radiography}");
	h3_1->SetXTitle("Deposited energy [keV]");
	h3_1->SetYTitle("Relative Counts");
	h3_1->Scale(1/h3_1->GetEntries());
	h3_1->Draw("HIST");
	c2->cd(2)->Update();

	TH1* h3_2 = static_cast<TH1D*>(F2->Get("histograms/LensEdepPerEvent"));
	h3_2->SetLineColor(kRed);
	h3_2->SetLineWidth(2);
	h3_2->Scale(1/h3_2->GetEntries());
	h3_2->Draw("HIST same");

	TH1* h3_3 = static_cast<TH1D*>(F3->Get("histograms/LensEdepPerEvent"));
	h3_3->SetLineColor(kBlue);
	h3_3->SetLineWidth(2);
	h3_3->Scale(1/h3_3->GetEntries());
	h3_3->Draw("HIST same");

	// --- End EdepPerEvent ---

	// --- DosePerEvent ---

	TH1* h4_1 = static_cast<TH1D*>(F1->Get("histograms/LensDosePerEvent"));
	h4_1->Scale(1/h4_1->GetEntries());
	h4_1->Draw("HIST same");

	Double_t ypos2 = c2->cd(2)->GetFrame()->GetY2();

	//draw an axis on the right side
	TGaxis *axis2 = new TGaxis(gPad->GetUxmin(),ypos2,
			gPad->GetUxmax(), ypos2,0,h4_1->GetXaxis()->GetBinCenter(h4_1->GetXaxis()->GetNbins()),510,"-");
	axis2->SetLineColor(kBlack);
	axis2->SetLabelColor(kBlack);
	axis2->SetTitleOffset(1.2);
	axis2->SetTitle("Absorbed dose [Gy]");
	axis2->Draw();

	TLegend* l2 = new TLegend(0.65, 0.70, 0.85, 0.85) ;
	l2->AddEntry(h3_1, "50kVp", "L") ;
	l2->AddEntry(h3_2, "60kVp", "L") ;
	l2->AddEntry(h3_3, "70kVp", "L") ;
	l2->Draw() ;

	// --- End DosePerEvent ---

	c2->Print("data/panoramic_002.png");

}
