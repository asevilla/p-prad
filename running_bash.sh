#!/bin/bash

./P-PRad mac/periapical_RX50kVp.mac > log/periapical_RX50kVp.log
sleep 2

./P-PRad mac/periapical_RX60kVp.mac > log/periapical_RX60kVp.log
sleep 2

./P-PRad mac/periapical_RX70kVp.mac > log/periapical_RX70kVp.log
sleep 2

./P-PRad mac/panoramic_RX50kVp.mac > log/panoramic_RX50kVp.log
sleep 2

./P-PRad mac/panoramic_RX60kVp.mac > log/panoramic_RX60kVp.log
sleep 2

./P-PRad mac/panoramic_RX70kVp.mac > log/panoramic_RX70kVp.log
sleep 2