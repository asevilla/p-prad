/*
 * P-P Rad 1.0
 * Copyright (c) 2019 Instituto Nacional de Cancerología - INC
 * All Right Reserved.
 *
 * Developed by Andrea Giraldo Torres
 *              Andrés Camilo Sevilla Moreno
 *
 * Use and copying of these libraries and preparation of derivative works
 * based upon these libraries are permitted. Any copy of these libraries
 * must include this copyright notice.
 *
 * Bogotá, Colombia.
 *
 */

//P2 Headers
#include "P2PrimaryGeneratorAction.hh"

P2PrimaryGeneratorAction::P2PrimaryGeneratorAction(): G4VUserPrimaryGeneratorAction()
{

	G4cout<<"01 - Primary Generator action have started !!!"<<G4endl;
	fParticleSource = new G4GeneralParticleSource();
}

P2PrimaryGeneratorAction::~P2PrimaryGeneratorAction()
{
  delete fParticleSource;
}

void P2PrimaryGeneratorAction::GeneratePrimaries(G4Event* anEvent)
{

  fParticleSource->GeneratePrimaryVertex(anEvent);

}
