/*
 * P-P Rad 1.0
 * Copyright (c) 2019 Instituto Nacional de Cancerología - INC
 * All Right Reserved.
 *
 * Developed by Andrea Giraldo Torres
 *              Andrés Camilo Sevilla Moreno
 *
 * Use and copying of these libraries and preparation of derivative works
 * based upon these libraries are permitted. Any copy of these libraries
 * must include this copyright notice.
 *
 * Bogotá, Colombia.
 *
 */

// Geant4 Headers
#include "G4RunManager.hh"
#include "G4EventManager.hh"
#include "G4SDManager.hh"
#include "G4HCofThisEvent.hh"
#include "G4UnitsTable.hh"
#include "G4SystemOfUnits.hh"

// P2 Headers
#include "P2EventAction.hh"
#include "P2Analysis.hh"
#include "P2RunAction.hh"

P2EventAction::P2EventAction(P2RunAction* runAction):
G4UserEventAction(),
fRunAction(runAction),
fThyroidEdep(0.),
fThyroidDose(0.),
fLensEdep(0.),
fLensDose(0.)
{}

P2EventAction::~P2EventAction()
{}

void P2EventAction::BeginOfEventAction(const G4Event* anEvent)
{
	G4int nEvents = G4RunManager::GetRunManager()->GetCurrentRun()->GetNumberOfEventToBeProcessed() ;
	if(nEvents>10){
		G4int 	fraction 	= G4int(nEvents/100) ;
		if(anEvent->GetEventID()%fraction == 0)
			G4cout<<"("<<(anEvent->GetEventID()/(nEvents*1.0)*100)<<" %)"<<G4endl ;

	}else {
		G4int 	fraction 	= G4int(nEvents/1) ;
		if(anEvent->GetEventID()%fraction == 0)
			G4cout<<"("<<(anEvent->GetEventID()/(nEvents*1.0)*100)<<" %)"<<G4endl ;
	}


	G4double KineticEnergyAtVertex = anEvent->GetPrimaryVertex()->GetPrimary()->GetKineticEnergy();

	// Analysis manager
	G4AnalysisManager* man = G4AnalysisManager::Instance();

	// Photon energy spectrum
	man->FillH1(0,KineticEnergyAtVertex);

	fThyroidEdep=0.;
	fThyroidDose=0.;

	fLensEdep=0.;
	fLensDose=0.;

}

void P2EventAction::EndOfEventAction(const G4Event* anEvent)
{

	if(!(fThyroidEdep>0. || fLensEdep>0.)) return;

	G4AnalysisManager* man = G4AnalysisManager::Instance();

	// accumulate statistics in run action
	// Analysis manager

	// Edep
	man->FillH1(1,fThyroidEdep);
	fRunAction->AddThyroidEdep(fThyroidEdep);
	//G4cout<<G4BestUnit(fThyroidEdep,"Energy")<<G4endl;
	man->FillH1(3,fLensEdep);
	fRunAction->AddLensEdep(fLensEdep);
	//G4cout<<G4BestUnit(fThyroidEdep,"Energy")<<G4endl;

	// Dose
	man->FillH1(2,fThyroidDose);
	fRunAction->AddThyroidDose(fThyroidDose);
	//G4cout<<G4BestUnit(fThyroidDose,"Dose")<<G4endl;
	man->FillH1(4,fLensDose);
	fRunAction->AddLensDose(fLensDose);
	//G4cout<<G4BestUnit(fLensDose,"Dose")<<G4endl;

}
