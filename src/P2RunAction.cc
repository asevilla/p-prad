/*
 * P-P Rad 1.0
 * Copyright (c) 2019 Instituto Nacional de Cancerología - INC
 * All Right Reserved.
 *
 * Developed by Andrea Giraldo Torres
 *              Andrés Camilo Sevilla Moreno
 *
 * Use and copying of these libraries and preparation of derivative works
 * based upon these libraries are permitted. Any copy of these libraries
 * must include this copyright notice.
 *
 * Bogotá, Colombia.
 *
 */

// Geant4 Headers
#include "G4RunManager.hh"
#include "G4Run.hh"
#include "G4AccumulableManager.hh"
#include "G4LogicalVolumeStore.hh"
#include "G4LogicalVolume.hh"
#include "G4UnitsTable.hh"
#include "G4SystemOfUnits.hh"

// P2 Headers
#include "P2RunAction.hh"
#include "P2Analysis.hh"

class G4AccumulableManager;

using namespace std;

P2RunAction::P2RunAction(): G4UserRunAction(),
		fThyroidEdep(0.),
		fThyroidEdep2(0.),
		fThyroidDose(0.),
		fThyroidDose2(0.),
		fLensEdep(0.),
		fLensEdep2(0.),
		fLensDose(0.),
		fLensDose2(0.)
{

	// add new units for dose
	//
	const G4double milligray = 1.e-3*gray;
	const G4double microgray = 1.e-6*gray;
	const G4double nanogray  = 1.e-9*gray;
	const G4double picogray  = 1.e-12*gray;

	new G4UnitDefinition("milligray", "milliGy" , "Dose", milligray);
	new G4UnitDefinition("microgray", "microGy" , "Dose", microgray);
	new G4UnitDefinition("nanogray" , "nanoGy"  , "Dose", nanogray);
	new G4UnitDefinition("picogray" , "picoGy"  , "Dose", picogray);

	G4AccumulableManager* accumulableManager = G4AccumulableManager::Instance();
	// Register accumulable to the accumulable manager
	accumulableManager->RegisterAccumulable(fThyroidEdep);
	accumulableManager->RegisterAccumulable(fThyroidEdep2);
	accumulableManager->RegisterAccumulable(fThyroidDose);
	accumulableManager->RegisterAccumulable(fThyroidDose2);
	accumulableManager->RegisterAccumulable(fLensEdep);
	accumulableManager->RegisterAccumulable(fLensEdep2);
	accumulableManager->RegisterAccumulable(fLensDose);
	accumulableManager->RegisterAccumulable(fLensDose2);

	G4AnalysisManager* analysisManager = G4AnalysisManager::Instance();
	analysisManager->SetActivation(true);
	G4cout << "Using " << analysisManager->GetType() << G4endl;
	analysisManager->SetVerboseLevel(0);
	analysisManager->SetFileName("data/P2");

	// Create Histograms an n-Tuples
	CreateHistos();
	CreateNTuples();


}

P2RunAction::~P2RunAction()
{
	delete G4AccumulableManager::Instance();
	delete G4AnalysisManager::Instance();
}

void P2RunAction::BeginOfRunAction(const G4Run*)
{

	//inform the runManager to save random number seed
	G4RunManager::GetRunManager()->SetRandomNumberStore(false);

	// reset accumulables to their initial values
	G4AccumulableManager* accumulableManager = G4AccumulableManager::Instance();
	accumulableManager->Reset();


	G4AnalysisManager* analysisManager = G4AnalysisManager::Instance();

	if(analysisManager->IsActive()){
		// Open an output file
		analysisManager->OpenFile();
	}


}

void P2RunAction::EndOfRunAction(const G4Run* run)
{
	G4int nofEvents = run->GetNumberOfEvent();
	if (nofEvents == 0) return;

	G4AnalysisManager* analysisManager = G4AnalysisManager::Instance();

	if(analysisManager->IsActive()){
		// Save histograms and ntuples
		analysisManager->Write();
		analysisManager->CloseFile();
	}


	// Merge accumulables
	G4AccumulableManager* accumulableManager = G4AccumulableManager::Instance();
	accumulableManager->Merge();

	// Thyroid
	// Compute dose = total energy deposit in a run and its variance
	//
	G4double thyroidDose=0., thyroidDose2=0., thyroidRmsDose=0.;

	thyroidDose=fThyroidDose.GetValue();
	thyroidDose2=fThyroidDose2.GetValue();

	thyroidRmsDose = thyroidDose2 - thyroidDose*thyroidDose/nofEvents;
	thyroidRmsDose = std::sqrt(thyroidRmsDose);

	// Thyroid
	// Compute dose = total energy deposit in a run and its variance
	//
	G4double thyroidEdep=0., thyroidEdep2=0., thyroidRmsEdep=0.;

	thyroidEdep=fThyroidEdep.GetValue();
	thyroidEdep2=fThyroidEdep2.GetValue();

	thyroidRmsEdep = thyroidEdep2 - thyroidEdep*thyroidEdep/nofEvents;
	thyroidRmsEdep = std::sqrt(thyroidRmsEdep);

	// Lens
	// Compute dose = total energy deposit in a run and its variance
	//
	G4double lensDose=0., lensDose2=0., lensRmsDose=0.;

	lensDose=fLensDose.GetValue();
	lensDose2=fLensDose2.GetValue();

	lensRmsDose = lensDose2 - lensDose*lensDose/nofEvents;
	lensRmsDose = std::sqrt(lensRmsDose);

	// Lens
	// Compute dose = total energy deposit in a run and its variance
	//
	G4double lensEdep=0., lensEdep2=0., lensRmsEdep=0.;

	lensEdep=fLensEdep.GetValue();
	lensEdep2=fLensEdep2.GetValue();

	lensRmsEdep = lensEdep2 - lensEdep*lensEdep/nofEvents;
	lensRmsEdep = std::sqrt(lensRmsEdep);

	// Print
	//
	if (IsMaster()) {
		G4cout
		<< G4endl
		<< "--------------------End of Global Run-----------------------";
		G4cout
		<< G4endl
		<< " Total events per run : "
		<< nofEvents
		<< G4endl;
		G4cout
		<< " Cumulated dose per event in thyroid tissue : "
		<< G4BestUnit(thyroidDose/nofEvents,"Dose") << " rms = " << G4BestUnit(thyroidRmsDose/nofEvents,"Dose")
		<< G4endl;
		G4cout
		<< " Cumulated Edep per event in thyroid tissue : "
		<< G4BestUnit(thyroidEdep/nofEvents,"Energy") << " rms = " << G4BestUnit(thyroidRmsEdep/nofEvents,"Energy")
		<< G4endl;
		G4cout
		<< " Cumulated dose per event in lens : "
		<< G4BestUnit(lensDose/nofEvents/2.,"Dose") << " rms = " << G4BestUnit(lensRmsDose/nofEvents/2.,"Dose")
		<< G4endl;
		G4cout
		<< " Cumulated Edep per event in lens : "
		<< G4BestUnit(lensEdep/nofEvents/2.,"Energy") << " rms = " << G4BestUnit(lensRmsEdep/nofEvents/2.,"Energy")
		<< G4endl;
	}

}

void P2RunAction::CreateNTuples(){
}

void P2RunAction::CreateHistos(){

	G4AnalysisManager* analysisManager = G4AnalysisManager::Instance();

	// Creating histos
	analysisManager->SetFirstHistoId(0);

	// id = 0
	analysisManager->CreateH1("PhotonEnergySpectrum","Photon energy spectrum (Thyroid)", 100, 0., 100*keV,"keV");
	analysisManager->SetH1Activation(0,false);
	analysisManager->SetH1XAxisTitle(0,"Kinetic Energy [keV]");
	analysisManager->SetH1YAxisTitle(0,"[Counts]");

	// id = 1
	analysisManager->CreateH1("ThyroidEdepPerEvent","Energy deposit per event (Thyroid)", 100, 0., 5E1*keV,"keV");
	analysisManager->SetH1Activation(0,false);
	analysisManager->SetH1XAxisTitle(0,"Energy Deposit [keV]");
	analysisManager->SetH1YAxisTitle(0,"[Counts]");

	// id = 2
	analysisManager->CreateH1("ThyroidDosePerEvent","Absorbed Dose per event (Thyroid)", 100, 0., 1.e-12*gray,"Gy");
	analysisManager->SetH1Activation(0,false);
	analysisManager->SetH1XAxisTitle(0,"Absorbed Dose [Gy]");
	analysisManager->SetH1YAxisTitle(0,"[Counts]");

	// id = 3
	analysisManager->CreateH1("LensEdepPerEvent","Energy deposit per event (Lens)", 100, 0., 5E1*keV,"keV");
	analysisManager->SetH1Activation(0,false);
	analysisManager->SetH1XAxisTitle(0,"Energy Deposit [keV]");
	analysisManager->SetH1YAxisTitle(0,"[Counts]");

	// id = 4
	analysisManager->CreateH1("LensDosePerEvent","Absorbed Dose per event (Lens)", 100, 0., 5.e-12*gray,"Gy");
	analysisManager->SetH1Activation(0,false);
	analysisManager->SetH1XAxisTitle(0,"Absorbed Dose [Gy]");
	analysisManager->SetH1YAxisTitle(0,"[Counts]");

}
