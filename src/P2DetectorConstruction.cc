/*
 * P-P Rad 1.0
 * Copyright (c) 2019 Instituto Nacional de Cancerología - INC
 * All Right Reserved.
 *
 * Developed by Andrea Giraldo Torres
 *              Andrés Camilo Sevilla Moreno
 *
 * Use and copying of these libraries and preparation of derivative works
 * based upon these libraries are permitted. Any copy of these libraries
 * must include this copyright notice.
 *
 * Bogotá, Colombia.
 *
 */

// Geant4 Headers
#include "P2DetectorConstruction.hh"

#include "G4SystemOfUnits.hh"
#include "G4Orb.hh"
#include "G4Box.hh"
#include "G4Tubs.hh"
#include "G4EllipticalTube.hh"
#include "G4Ellipsoid.hh"
#include "G4TwistedTrap.hh"
#include "G4Trd.hh"
#include "G4UnionSolid.hh"
#include "G4SubtractionSolid.hh"
#include "G4Colour.hh"
#include "G4LogicalVolume.hh"
#include "G4VisAttributes.hh"
#include "G4PVPlacement.hh"
#include "G4NistManager.hh"

// NP1 Headers

P2DetectorConstruction::P2DetectorConstruction()
: G4VUserDetectorConstruction()
{}

P2DetectorConstruction::~P2DetectorConstruction()
{}

G4VPhysicalVolume* P2DetectorConstruction::Construct()
{  

	// Get nist material manager
	G4NistManager* nist = G4NistManager::Instance();

	// Define materials
	G4Material* vacuum = nist->FindOrBuildMaterial("G4_Galactic");

	// General Attributes
	G4VisAttributes* simpleInvisibleSVisAtt;
	simpleInvisibleSVisAtt= new G4VisAttributes(G4Colour(0.,0.5,0.5,0.1));
	simpleInvisibleSVisAtt->SetVisibility(false);

	// Geometrical Volume =========================================================================================

	// World
	G4double world_size_X = (10/2.)*m;
	G4VSolid* world_geo = new G4Box("world_geo", world_size_X, world_size_X, world_size_X);

	//Logical Volume ==============================================================================================

	// World
	G4LogicalVolume* world_log = new G4LogicalVolume(world_geo,vacuum,"world_log");
	world_log -> SetVisAttributes(simpleInvisibleSVisAtt);

	//Physics Volume  =============================================================================================

	G4VPhysicalVolume* world_phys =
			new G4PVPlacement(0,                    //no rotation
					G4ThreeVector(),       			//at (0,0,0)
					world_log,      				//its logical volume
					"world_phys",   				//its name
					0,                     			//its mother  volume
					false,                 			//no boolean operation
					0,                     			//copy number
					0);      			    		//overlaps checking

	SetupGeometry(world_log);

	//always return the physical World
	//
	return world_phys;

}

void P2DetectorConstruction::SetupGeometry(G4LogicalVolume* motherVolume){

	// Get nist material manager
	G4NistManager* nist = G4NistManager::Instance();

	// Define materials
	G4Material* water = nist->FindOrBuildMaterial("G4_WATER");

	// Phantom Vis Attributes
	G4VisAttributes* phantomVisAtt;
	phantomVisAtt= new G4VisAttributes(G4Colour(0.0,1.0,1.0,1.0));
	phantomVisAtt->SetVisibility(true);

	// Thyroid Vis Attributes
	G4VisAttributes* thyroidVisAtt;
	thyroidVisAtt= new G4VisAttributes(G4Colour(1.0,0.0,0.0,1.0));
	thyroidVisAtt->SetVisibility(true);

	// Len Vis Attributes
	G4VisAttributes* lenVisAtt;
	lenVisAtt= new G4VisAttributes(G4Colour(1.0,0.0,0.0,1.0));
	lenVisAtt->SetVisibility(true);


	// Geometrical Volume =========================================================================================

	// Head Ellipsoid 1
	G4double headEllipsoid1_dx = 8*cm;
	G4double headEllipsoid1_dy = 10*cm;
	G4double headEllipsoid1_dz = 7*cm;
	G4VSolid* headEllipsoid1_geo = new G4Ellipsoid("headEllipsoid1_geo", headEllipsoid1_dx, headEllipsoid1_dy, headEllipsoid1_dz, 0, 0);

	// Head Tub 1
	G4double headTub1_dx = 8*cm;
	G4double headTub1_dy = 10*cm;
	G4double headTub1_dz = (10/2.)*cm;
	G4VSolid* headTub1_geo = new G4EllipticalTube("headTub1_geo", headTub1_dx, headTub1_dy, headTub1_dz);

	// Head Tub 2
	G4double headTub2_rmax = 5*cm;
	G4double headTub2_dz = (2/2.)*cm;
	G4double headTub2_pDPhi = 360*deg;
	G4VSolid* headTub2_geo = new G4Tubs("headTub2_geo", 0*cm,headTub2_rmax,headTub2_dz,0*deg,headTub2_pDPhi);

	// Head Ellipsoid 2
	G4double headEllipsoid2_dx = 1.5*cm;
	G4double headEllipsoid2_dy = 5*cm;
	G4double headEllipsoid2_dz = 2.5*cm;
	G4VSolid* headEllipsoid2_geo = new G4Ellipsoid("headEllipsoid_geo", headEllipsoid2_dx, headEllipsoid2_dy, headEllipsoid2_dz, 0, 0);

	// Head Ellipsoid 3
	G4double headEllipsoid3_dx = 4.0*cm;
	G4double headEllipsoid3_dy = 5*cm;
	G4double headEllipsoid3_dz = 3.0*cm;
	G4VSolid* headEllipsoid3_geo = new G4Ellipsoid("headEllipsoid_geo", headEllipsoid3_dx, headEllipsoid3_dy, headEllipsoid3_dz, 0, 0);

	// Eye Orb 1
	G4double eyeOrb1_rmax = (2.5/2.)*cm;
	G4VSolid* eyeOrb1_geo = new G4Orb("eyeOrb1_geo", eyeOrb1_rmax);

	// Shoulders Top Trapezoid
	G4double shoulderTrd1_dy1 =  8*cm;
	G4double shoulderTrd1_dy2 = 6.5*cm;
	G4double shoulderTrd1_dx1 = 19*cm;
	G4double shoulderTrd1_dx2 = 14*cm;
	G4double shoulderTrd1_dz = 2.5*cm;
	G4VSolid* shoulderTrd1_geo= new G4Trd("shoulderTrd1_geo",shoulderTrd1_dx1,shoulderTrd1_dx2,shoulderTrd1_dy1,shoulderTrd1_dy2,shoulderTrd1_dz);

	// Shoulders Bottom Trapezoid
	G4double shoulderTrd2_dy1 =  10*cm;
	G4double shoulderTrd2_dy2 = 8*cm;
	G4double shoulderTrd2_dx1 = 20*cm;
	G4double shoulderTrd2_dx2 = 19*cm;
	G4double shoulderTrd2_dz = 10*cm;
	G4VSolid* shoulderTrd2_geo= new G4Trd("shoulderTrd2_geo",shoulderTrd2_dx1,shoulderTrd2_dx2,shoulderTrd2_dy1,shoulderTrd2_dy2,shoulderTrd2_dz);

	// Trunk Tub 1
	G4double trunkTub1_dx = 20*cm;
	G4double trunkTub1_dy = 10*cm;
	G4double trunkTub1_dz = 10*cm;
	G4VSolid* trunkTub1_geo = new G4EllipticalTube("trunkTub1_geo", trunkTub1_dx, trunkTub1_dy, trunkTub1_dz);

	// Head
	//
	G4ThreeVector zTrans1(0, 0, headTub1_dz);
	G4UnionSolid* head_geo = new G4UnionSolid("head_geo", headTub1_geo,headEllipsoid1_geo, 0, zTrans1);
	//
	G4ThreeVector zTrans2(0, 0, -headTub1_dz);
	head_geo = new G4UnionSolid("head_geo", head_geo, headEllipsoid1_geo, 0, zTrans2);
	//
	G4ThreeVector zTrans3(0, 0, -(headTub1_dz+headEllipsoid1_dz));
	head_geo = new G4UnionSolid("head_geo", head_geo, headTub2_geo, 0, zTrans3);
	//
	G4double theta1 = -30*deg;
	G4ThreeVector zTrans4(-headTub1_dx*sin(theta1), -headTub1_dy*cos(theta1), 0);
	G4RotationMatrix* zRot1 = new G4RotationMatrix();
	zRot1->rotateZ(theta1+90*deg);
	G4SubtractionSolid* head_geo1 = new G4SubtractionSolid("head_geo", head_geo, headEllipsoid2_geo, zRot1, zTrans4);
	//
	G4double theta = 30*deg;
	G4ThreeVector zTrans5(-headTub1_dx*sin(theta), -headTub1_dy*cos(theta), 0);
	G4RotationMatrix* zRot2 = new G4RotationMatrix();
	zRot2->rotateZ(theta+90*deg);
	G4SubtractionSolid* head_geo2 = new G4SubtractionSolid("head_geo", head_geo1, headEllipsoid2_geo, zRot2, zTrans5);
	//
	G4ThreeVector zTrans6(0., -headTub1_dx*3/4., -(headTub1_dz+headEllipsoid1_dz/5.));
	G4SubtractionSolid* head_geo3 = new G4SubtractionSolid("head_geo", head_geo2, headEllipsoid3_geo, 0, zTrans6);
	//
	G4ThreeVector zTrans7(-headTub1_dx*sin(theta1)*0.9, -headTub1_dy*cos(theta1)*0.8, 0);
	head_geo = new G4UnionSolid("head_geo", head_geo3, eyeOrb1_geo, 0, zTrans7);
	//
	G4ThreeVector zTrans8(-headTub1_dx*sin(theta)*0.9, -headTub1_dy*cos(theta)*0.8, 0);
	head_geo = new G4UnionSolid("head_geo", head_geo, eyeOrb1_geo, 0, zTrans8);
	//
	//G4ThreeVector zTrans9(0, 0, -(headTub1_dz+headEllipsoid1_dz+headTub2_dz+shoulderTrd1_dz));
	//head_geo = new G4UnionSolid("head_geo", head_geo, shoulderTrd1_geo, 0, zTrans9);
	//
	//G4ThreeVector zTrans10(0, 0, -(headTub1_dz+headEllipsoid1_dz+headTub2_dz+2*shoulderTrd1_dz+shoulderTrd2_dz));
	//head_geo= new G4UnionSolid("head_geo", head_geo, shoulderTrd2_geo, 0, zTrans10);
	//
	G4ThreeVector zTrans9(0, 0, -(headTub1_dz+headEllipsoid1_dz+headTub2_dz+trunkTub1_dz));
	head_geo = new G4UnionSolid("head_geo", head_geo, trunkTub1_geo, 0, zTrans9);

	// Thyroid Trapezoid
	G4double headTrdr_dy2 =(1.5/2.)*cm;
	G4double headTrdr_dy1= (1/2.)*cm;
	G4double headTrdr_dx2= (4/2.)*cm;
	G4double headTrdr_dx1= (2.5/2.)*cm;
	G4double headTrdr_dz= (3/2.)*cm;
	G4VSolid* thyroid_geo =new G4Trd("headTrdr_geo",headTrdr_dx1, headTrdr_dx2, headTrdr_dy1,headTrdr_dy2,headTrdr_dz);

	// Len Ellipsoid 1
	G4double lenEllipsoid1_dx = 9*mm;
	G4double lenEllipsoid1_dy = 5*mm;
	G4double lenEllipsoid1_dz = 9*mm;
	G4VSolid* leftLen_geo = new G4Ellipsoid("leftLen_geo", lenEllipsoid1_dx, lenEllipsoid1_dy, lenEllipsoid1_dz, 0, 0);

	// Len Ellipsoid 2
	G4double lenEllipsoid2_dx = 9*mm;
	G4double lenEllipsoid2_dy = 5*mm;
	G4double lenEllipsoid2_dz = 9*mm;
	G4VSolid* rightLen_geo = new G4Ellipsoid("rightLen_geo", lenEllipsoid2_dx, lenEllipsoid2_dy, lenEllipsoid2_dz, 0, 0);

	//Logical Volume ==============================================================================================

	// Head
	G4LogicalVolume* head_log = new G4LogicalVolume(head_geo,water,"head_log");
	head_log -> SetVisAttributes(phantomVisAtt);

	// Thyroid
	G4LogicalVolume* thyroid_log = new G4LogicalVolume(thyroid_geo,water,"thyroid_log");
	thyroid_log -> SetVisAttributes(thyroidVisAtt);

	fScoringVolumeVector.push_back(thyroid_log);

	// Left Len
	G4LogicalVolume* leftLen_log = new G4LogicalVolume(leftLen_geo,water,"leftLen_log");
	leftLen_log -> SetVisAttributes(lenVisAtt);

	fScoringVolumeVector.push_back(leftLen_log);

	// Right Len
	G4LogicalVolume* rightLen_log = new G4LogicalVolume(rightLen_geo,water,"rightLen_log");
	rightLen_log -> SetVisAttributes(lenVisAtt);

	fScoringVolumeVector.push_back(rightLen_log);

	//Physics Volume  =============================================================================================

	// Head
	new G4PVPlacement(0,                     					//no rotation
			G4ThreeVector(),       								//at (0,0,0)
			head_log,	     									//its logical volume
			"head_phys", 		 								//its name
			motherVolume, 										//its mother  volume
			false,                 								//no boolean operation
			0,                     								//copy number
			true);      			     						//overlaps checking

	// Thyroid Position
	G4double thyroid_y = -(shoulderTrd1_dy1+shoulderTrd1_dy2)/2.*3/4.;
	G4double thyroid_z = -(headTub1_dz+headEllipsoid1_dz+headTub2_dz+shoulderTrd1_dz);

	// Thyroid
	new G4PVPlacement(0,                     					//no rotation
			G4ThreeVector(0,thyroid_y,thyroid_z),				//at (0,0,0)
			thyroid_log,	     								//its logical volume
			"thyroid_phys", 		 							//its name
			head_log, 											//its mother  volume
			false,                 								//no boolean operation
			0,                     								//copy number
			true);      			     						//overlaps checking

	// Left Len Position
	G4ThreeVector zTrans11(-headTub1_dx*sin(theta1)*0.95, -headTub1_dy*cos(theta1)*0.85, 0);

	// Left Len
	new G4PVPlacement(0,                     					//no rotation
			zTrans11,											//at (0,0,0)
			leftLen_log,	     									//its logical volume
			"leftLen_phys", 		 							//its name
			head_log, 											//its mother  volume
			false,                 								//no boolean operation
			0,                     								//copy number
			true);      			     						//overlaps checking

	// Right Len Position
	G4ThreeVector zTrans12(-headTub1_dx*sin(theta)*0.95, -headTub1_dy*cos(theta)*0.85, 0);

	// Right Len
	new G4PVPlacement(0,                     					//no rotation
			zTrans12,											//at (0,0,0)
			rightLen_log,	     									//its logical volume
			"rightLen_phys", 		 							//its name
			head_log, 											//its mother  volume
			false,                 								//no boolean operation
			0,                     								//copy number
			true);      			     						//overlaps checking

}


