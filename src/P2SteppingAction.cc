/*
 * P-P Rad 1.0
 * Copyright (c) 2019 Instituto Nacional de Cancerología - INC
 * All Right Reserved.
 *
 * Developed by Andrea Giraldo Torres
 *              Andrés Camilo Sevilla Moreno
 *
 * Use and copying of these libraries and preparation of derivative works
 * based upon these libraries are permitted. Any copy of these libraries
 * must include this copyright notice.
 *
 * Bogotá, Colombia.
 *
 */

// Geant4 Headers
#include "G4RunManager.hh"
#include "G4UnitsTable.hh"
#include "G4SystemOfUnits.hh"

// P2 Headers
#include "P2SteppingAction.hh"
#include "P2EventAction.hh"
#include "P2Analysis.hh"
#include "P2DetectorConstruction.hh"
#include "G4VPhysicalVolume.hh"

using namespace std;

P2SteppingAction::P2SteppingAction(P2EventAction* eventAction)
: G4UserSteppingAction(),
  fEventAction(eventAction),
  fScoringVolumeVector(0)
{}

P2SteppingAction::~P2SteppingAction()
{}

void P2SteppingAction::UserSteppingAction(const G4Step* aStep)
{

	if (fScoringVolumeVector.empty()) {
		const P2DetectorConstruction* detectorConstruction
		= static_cast<const P2DetectorConstruction*>
		(G4RunManager::GetRunManager()->GetUserDetectorConstruction());
		fScoringVolumeVector = detectorConstruction->GetScoringVolumeVector();
	}

	// get volume of the current step
	G4LogicalVolume* currentVolume
	= aStep->GetPreStepPoint()->GetTouchableHandle()
	->GetVolume()->GetLogicalVolume();

	G4bool IsScoringVolume = false;

	// check if we are in scoring volume
	for (unsigned i=0; i < fScoringVolumeVector.size(); i++) {
		if (currentVolume == fScoringVolumeVector[i]) IsScoringVolume = true;
	}

	if(!IsScoringVolume) return;

	if(aStep->GetTotalEnergyDeposit()==0) return;

	G4double Edep = aStep->GetTotalEnergyDeposit();
	G4double Dose = aStep->GetTotalEnergyDeposit()/currentVolume->GetMass();

	if(currentVolume->GetName()=="thyroid_log"){
		fEventAction->AddThyroidEdep(Edep);
		fEventAction->AddThyroidDose(Dose);
	}

	if(currentVolume->GetName()=="leftLen_log" || currentVolume->GetName()=="rightLen_log"){
		fEventAction->AddLensEdep(Edep);
		fEventAction->AddLensDose(Dose);
	}

}
