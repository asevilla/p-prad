/*
 * P-P Rad 1.0
 * Copyright (c) 2019 Instituto Nacional de Cancerología - INC
 * All Right Reserved.
 *
 * Developed by Andrea Giraldo Torres
 *              Andrés Camilo Sevilla Moreno
 *
 * Use and copying of these libraries and preparation of derivative works
 * based upon these libraries are permitted. Any copy of these libraries
 * must include this copyright notice.
 *
 * Bogotá, Colombia.
 *
 */

#include "P2ActionInitialization.hh"
#include "P2PrimaryGeneratorAction.hh"
#include "P2RunAction.hh"
#include "P2EventAction.hh"
#include "P2SteppingAction.hh"


P2ActionInitialization::P2ActionInitialization()
: G4VUserActionInitialization()
{}

P2ActionInitialization::~P2ActionInitialization()
{}

void P2ActionInitialization::BuildForMaster() const
{
	SetUserAction(new P2RunAction());
}

void P2ActionInitialization::Build() const
{

	P2RunAction* runAction = new P2RunAction();
	SetUserAction(runAction);

	SetUserAction(new P2PrimaryGeneratorAction);

	P2EventAction* eventAction = new P2EventAction(runAction);
	SetUserAction(eventAction);

	P2SteppingAction* steppingAction = new P2SteppingAction(eventAction);
	SetUserAction(steppingAction);

}  
