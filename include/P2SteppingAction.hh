/*
 * P-P Rad 1.0
 * Copyright (c) 2019 Instituto Nacional de Cancerología - INC
 * All Right Reserved.
 *
 * Developed by Andrea Giraldo Torres
 *              Andrés Camilo Sevilla Moreno
 *
 * Use and copying of these libraries and preparation of derivative works
 * based upon these libraries are permitted. Any copy of these libraries
 * must include this copyright notice.
 *
 * Bogotá, Colombia.
 *
 */

#ifndef P2SteppingAction_h
#define P2SteppingAction_h 1

// Geant4 Headers
#include "G4UserSteppingAction.hh"
#include "globals.hh"
#include "G4LogicalVolume.hh"

using namespace std;

class P2EventAction;

/// Stepping action class
///

class P2SteppingAction : public G4UserSteppingAction
{

public:
	P2SteppingAction(P2EventAction* eventAction);
	virtual ~P2SteppingAction();

	// method from the base class
	virtual void UserSteppingAction(const G4Step*);

private:
	P2EventAction*  			fEventAction;
	vector<G4LogicalVolume*> 	fScoringVolumeVector;

};

#endif
