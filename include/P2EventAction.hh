/*
 * P-P Rad 1.0
 * Copyright (c) 2019 Instituto Nacional de Cancerología - INC
 * All Right Reserved.
 *
 * Developed by Andrea Giraldo Torres
 *              Andrés Camilo Sevilla Moreno
 *
 * Use and copying of these libraries and preparation of derivative works
 * based upon these libraries are permitted. Any copy of these libraries
 * must include this copyright notice.
 *
 * Bogotá, Colombia.
 *
 */

#ifndef P2EventAction_h
#define P2EventAction_h 1

// Geant4 Headers
#include "G4UserEventAction.hh"
#include "globals.hh"
#include "G4Run.hh"
#include "G4Event.hh"
#include "G4SystemOfUnits.hh"

// Root Headers
#include "TH1.h"

class P2RunAction;

using namespace std;

/// Event action class
///

class P2EventAction : public G4UserEventAction
{
  public:
    P2EventAction(P2RunAction* runAction);
    virtual ~P2EventAction();

    virtual void BeginOfEventAction(const G4Event* event);
    virtual void EndOfEventAction(const G4Event* event);

    void AddThyroidEdep(G4double Edep) { fThyroidEdep += Edep; }
    void AddThyroidDose(G4double dose) { fThyroidDose += dose; }

    void AddLensEdep(G4double Edep) { fLensEdep += Edep; }
    void AddLensDose(G4double dose) { fLensDose += dose; }

  private:
    P2RunAction* 			fRunAction;
    G4double     			fThyroidEdep;
    G4double     			fThyroidDose;
    G4double     			fLensEdep;
    G4double     			fLensDose;

};

#endif


