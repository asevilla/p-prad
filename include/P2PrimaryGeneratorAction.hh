/*
 * P-P Rad 1.0
 * Copyright (c) 2019 Instituto Nacional de Cancerología - INC
 * All Right Reserved.
 *
 * Developed by Andrea Giraldo Torres
 *              Andrés Camilo Sevilla Moreno
 *
 * Use and copying of these libraries and preparation of derivative works
 * based upon these libraries are permitted. Any copy of these libraries
 * must include this copyright notice.
 *
 * Bogotá, Colombia.
 *
 */

#ifndef P2PrimaryGeneratorAction_h
#define P2PrimaryGeneratorAction_h 1

// Geant4 Headers
#include "G4VUserPrimaryGeneratorAction.hh"
#include "G4GeneralParticleSource.hh"
#include "globals.hh"

class G4Event;
class G4Box;

class P2PrimaryGeneratorAction : public G4VUserPrimaryGeneratorAction
{
  public:
    P2PrimaryGeneratorAction();
    virtual ~P2PrimaryGeneratorAction();

    // method from the base class
    virtual void GeneratePrimaries(G4Event*);

    inline const G4GeneralParticleSource* GetParticleSource() const {return fParticleSource; } ;

  private:
	G4GeneralParticleSource* fParticleSource;
};


#endif
