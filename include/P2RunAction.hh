/*
 * P-P Rad 1.0
 * Copyright (c) 2019 Instituto Nacional de Cancerología - INC
 * All Right Reserved.
 *
 * Developed by Andrea Giraldo Torres
 *              Andrés Camilo Sevilla Moreno
 *
 * Use and copying of these libraries and preparation of derivative works
 * based upon these libraries are permitted. Any copy of these libraries
 * must include this copyright notice.
 *
 * Bogotá, Colombia.
 *
 */

#ifndef P2RunAction_h
#define P2RunAction_h 1

// Geant4 Headers
#include "G4UserRunAction.hh"
#include "G4Accumulable.hh"
#include "globals.hh"

// P2 Headers
#include "P2DetectorConstruction.hh"

using namespace std;

/// Run action class
///

class P2RunAction : public G4UserRunAction
{

  public:
    P2RunAction();
    virtual ~P2RunAction();

    virtual void BeginOfRunAction(const G4Run*);
    virtual void EndOfRunAction(const G4Run*);

    void CreateHistos();
    void CreateNTuples();

    inline void AddThyroidEdep(G4double Edep) {fThyroidEdep+=Edep; fThyroidEdep2+=Edep*Edep;}
    inline void AddThyroidDose(G4double dose) {fThyroidDose+=dose; fThyroidDose2+=dose*dose;}

    inline void AddLensEdep(G4double Edep) {fLensEdep+=Edep; fLensEdep2+=Edep*Edep;}
    inline void AddLensDose(G4double dose) {fLensDose+=dose; fLensDose2+=dose*dose;}

private:
    G4Accumulable<G4double>     		fThyroidEdep;
    G4Accumulable<G4double>	   			fThyroidEdep2;
    G4Accumulable<G4double>     		fThyroidDose;
    G4Accumulable<G4double>	   			fThyroidDose2;

    G4Accumulable<G4double>     		fLensEdep;
    G4Accumulable<G4double>	   			fLensEdep2;
    G4Accumulable<G4double>     		fLensDose;
    G4Accumulable<G4double>	   			fLensDose2;

};

#endif // P2RunAction_h
