/*
 * P-P Rad 1.0
 * Copyright (c) 2019 Instituto Nacional de Cancerología - INC
 * All Right Reserved.
 *
 * Developed by Andrea Giraldo Torres
 *              Andrés Camilo Sevilla Moreno
 *
 * Use and copying of these libraries and preparation of derivative works
 * based upon these libraries are permitted. Any copy of these libraries
 * must include this copyright notice.
 *
 * Bogotá, Colombia.
 *
 */

#ifndef P2DetectorConstruction_h
#define P2DetectorConstruction_h 1

// Geant4 Headers
#include "G4VUserDetectorConstruction.hh"
#include "G4LogicalVolume.hh"

using namespace std;

/// Detector construction class to define materials and geometry.

class P2DetectorConstruction : public G4VUserDetectorConstruction
{
  public:
    P2DetectorConstruction();
    virtual ~P2DetectorConstruction();

    virtual G4VPhysicalVolume* Construct();
    void SetupGeometry(G4LogicalVolume* motherVolume);
    inline vector<G4LogicalVolume*> GetScoringVolumeVector() const { return fScoringVolumeVector; }

  private:
    vector<G4LogicalVolume*>	fScoringVolumeVector;

};

#endif // P2DetectorConstruction_h
