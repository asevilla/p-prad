/*
 * P-P Rad 1.0
 * Copyright (c) 2019 Instituto Nacional de Cancerología - INC
 * All Right Reserved.
 *
 * Developed by Andrea Giraldo Torres
 *              Andrés Camilo Sevilla Moreno
 *
 * Use and copying of these libraries and preparation of derivative works
 * based upon these libraries are permitted. Any copy of these libraries
 * must include this copyright notice.
 *
 * Bogotá, Colombia.
 *
 */

#ifndef P2ActionInitialization_h
#define P2ActionInitialization_h 1

#include "G4VUserActionInitialization.hh"

/// Action initialization class.

class P2ActionInitialization : public G4VUserActionInitialization
{
  public:
    P2ActionInitialization();
    virtual ~P2ActionInitialization();

    virtual void BuildForMaster() const;
    virtual void Build() const;
};

#endif //P2ActionInitialization_h

    
